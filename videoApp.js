/**
 * Created with JetBrains PhpStorm.
 * Creator: Darcy Pleckham
 */


var videoApp = angular.module('videoApp', []);

// to change video, specify the url below
var vsource = 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4';



var videoDuration = 0;
var videoObj = document.getElementById("mainVideo");
videoObj.src = vsource;




// Controlling the Video and setting the properties
videoApp.controller('videoController', function ($scope, ListService) {

    var videoObj = document.getElementById("mainVideo");
    var videoObjDuration = document.getElementById("videoDuration");
    var videoForm = document.getElementById("videoForm");
    var videoContainer = document.getElementById("videoContainer");
    var videoLoader = document.getElementById("videoLoader");
    var videoSource = videoObj.src;

    videoObj.load();


    var i = setInterval(function() {
        if(videoObj.readyState > 0) {
            var minutes = parseInt(videoObj.duration / 60, 10);
            var seconds = videoObj.duration % 60;
            var totalSeconds = Math.round(seconds);

            // (Put the minutes and seconds in the display)

            videoObjDuration.innerHTML = totalSeconds;

            document.getElementById('clipStart').max = totalSeconds;
            document.getElementById('clipEnd').max = totalSeconds;
            videoDuration = totalSeconds;

            videoForm.style.display = 'block';
            videoContainer.style.display = 'block';
            videoLoader.style.display = 'none';
            clearInterval(i);
        }
    }, 200);



});



// Form Controller
videoApp.controller('videoFormController', function ($scope, ListService) {


        $scope.close = function () {

            document.getElementById('videoFormLayer').style.display = 'none';
            return false;
        };





    $scope.videoFormSubmit = function (clip) {


        if(!document.getElementById('clipName').value){
            alert('please enter clip name');
            document.getElementById('clipName').focus();
            return;
        }
        if(!document.getElementById('clipStart').value){
            alert('please enter clip start time');
            document.getElementById('clipStart').focus();
            return;
        }
        if(!document.getElementById('clipEnd').value){
            alert('please enter clip end time');
            document.getElementById('clipEnd').focus();
            return;
        }
        if(document.getElementById('clipStart').value >= document.getElementById('clipEnd').value){
            alert('Start time must be before End Time');
            document.getElementById('clipStart').focus();
            return;
        }

        // update an existing item
        if(document.getElementById('videoFormAction').value == 'update'){


            $scope.videoForm.clipKey = document.getElementById('clipKey').value;




            var itemsArray = JSON.parse(localStorage.getItem("items"));

           var  newItem = itemsArray[$scope.videoForm.clipKey];
            if(clip){
                if(clip.name){
                    newItem[0] = clip.name;
                }
                if(clip.start){
                    newItem[1] = clip.start;
                }
                if(clip.end){
                    newItem[2] = clip.end;
                }
            }

            itemsArray[$scope.videoForm.clipKey] = newItem;
            localStorage.setItem("items", JSON.stringify(itemsArray));



        }else{
// New items
            var  newItem = [clip.name,clip.start,clip.end];

            if(localStorage.getItem("items") === null){
                // if items are not yet set
                var items = [];
                items[0] = newItem;
                localStorage.setItem("items", JSON.stringify(items));

            }else{

                // if items are existing push the new item to the end
                var itemsArray = JSON.parse(localStorage.getItem("items"));
                itemsArray.push(newItem);
                localStorage.setItem("items", JSON.stringify(itemsArray));

            }
        }




        // clear fields
        document.getElementById('clipName').value = '';
        document.getElementById('clipStart').value = '';
        document.getElementById('clipEnd').value = '';
        document.getElementById('videoFormLayer').style.display = 'none';
        ListService.displayList();




    };






});




// Controller for initializing the list
videoApp.controller('clipListController', function ($scope, ListService) {
    ListService.displayList();

});




// Controller for the Top of the list
videoApp.controller('listTopController', function ($scope) {

    $scope.new = function () {
        document.getElementById('videoFormTitle').innerHTML = 'New Clip';
        document.getElementById('videoFormAction').value = 'create';
        document.getElementById('videoFormLayer').style.display = 'block';
        document.getElementById('clipName').focus();
        return false;
    };

    $scope.play = function () {

        videoObj.currentTime = 0;
         videoObj.play();
    }

});





// Factory used for the clip listing and called when a change occurs
    videoApp.factory("ListService", function() {



return {


    displayList: function () {


        var videoObj = document.getElementById("mainVideo");
        var videoSource = videoObj.src;
        var rootObj = this;

        if (typeof(Storage) !== "undefined") {
            if(localStorage.getItem("items") !== null){
                var items = JSON.parse(localStorage.getItem("items"));
                var displayHtml = '<ul>';

                for(var i=0; i < items.length; i++){

                    var displayHtml = displayHtml + '<li>' +

                        '<button class="iconButton playButton" value="' + i +
                        '" data-start="' + items[i][1] + '"  data-end="' + items[i][2] + '" > <img src="images/icon_play.png" alt="play" title="play"/> ' +'</button>' +

                        '<button class="iconButton editButton" value="' + i +
                        '" data-start="' + items[i][1] + '"  data-end="' + items[i][2] + '" > <img src="images/icon_edit.png" alt="edit" title="edit" />  ' + '</button>' +

                        items[i][0] + ' ('+ items[i][1] + ',' + items[i][2] + ')' +

                        '<button class="iconButton removeButton" title="delete" value="' + i +
                        '" data-start="' + items[i][1] + '"  data-end="' + items[i][2] + '" >x ' + '</button>' + '</li>';
                }

                var displayHtml = displayHtml + '</ul><br> '+ items.length + ' items in list';


                document.getElementById('listArea').innerHTML = displayHtml;

                $( ".removeButton" ).bind( "click", function() {

                   var confm =  confirm('Are you sure you want to delete this clip');
                    if (confm == true) {
                        var itemsArray = JSON.parse(localStorage.getItem("items"));
                        itemsArray.splice($( this ).val(),1);
                        localStorage.setItem("items", JSON.stringify(itemsArray));
                        rootObj.displayList();
                    }


                });

                $( ".playButton" ).bind( "click", function() {

                    var start = $(this).attr('data-start');
                    var end = $(this).attr('data-end');
                    document.getElementById("mainVideo").src = vsource + '#t=' + start + ',' + end;

                    videoObj.play();
                    videoObj.focus();

                });


                $( ".editButton" ).bind( "click", function() {


                    var id = $(this).attr('value');
                    var itemsArray = JSON.parse(localStorage.getItem("items"));

                    var clipKey= id;
                    var clipName = itemsArray[id][0];
                    var clipStart = itemsArray[id][1];
                    var clipEnd = itemsArray[id][2];


                    document.getElementById('clipName').value = clipName;
                    document.getElementById('clipStart').value = clipStart;
                    document.getElementById('clipEnd').value = clipEnd;
                    document.getElementById('clipKey').value = clipKey;

                    document.getElementById('videoFormLayer').style.display = 'block';

                    document.getElementById('videoFormTitle').innerHTML = 'Update Clip #' + (parseInt(id) + 1);

                    document.getElementById('clipButton').innerHTML = 'Update Clip';
                    document.getElementById('videoFormAction').value = 'update';
                    document.getElementById('clipName').focus();



                });



                document.getElementById('clipName').focus();
            }
        }else{

            alert('Please update your browser. Your current browser does not support Web Storage.')
        }
    }

};






});

